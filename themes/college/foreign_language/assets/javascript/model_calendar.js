var app = angular.module('demo', ['ui.bootstrap']);

app.controller('MainCtrl', ['$scope', '$modal', function($scope, $modal){
    var $ctrl, disabled;
    $ctrl = this;
  $ctrl.open = function () {

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl'
    });

  };

}]);

app.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', function ($scope, $modalInstance) {

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}]);
