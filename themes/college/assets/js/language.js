(function($) {
    "use strict";
    var x = [$('.eqh-1'), $('.eqh-2'), $('.eqh-3'), $('.eqh-4')];
    var y = [$('.img-eqh-1'), $('.img-eqh-2'), $('.img-eqh-3'), $('.img-eqh-4')];
    var initmode;
    $(window).bind('load resize', function() {
        $.each(x, function() {
            $(this).css('height', 'auto');
            var i = 0;
            $.each($(this), function() {
                var h = $(this).outerHeight();
                if (i < h) {
                    i = h;
                } else {
                    i = i;
                }
            });
            $(this).css('height', i + 'px');
        });

        $.each(y, function() {
            $(this).css('height', 'auto');
            var j = 5000;
            $.each($(this), function() {
                var h = $(this).outerHeight();
                if (j > h) {
                    j = h;
                } else {
                    j = j;
                }
            });
            $(this).css('height', j + 'px');
        });
        
        $.each($('.side_menu li.has_sub > .sub_menu2'), function() {
            $(this).css({
                'position': 'static',
                '-webkit-transition': 'all .3s ease-in-out',
                '-o-transition': 'all .3s ease-in-out',
                'transition': 'all .3s ease-in-out'
            });
        });

        // var top_bar_h = $(".top_bar").outerHeight();
        // $(".top_bar").addClass("top_bar_close").css("margin-top", (-top_bar_h + 9) + "px");
    });

    $(window).load(function(){
        $('.portfolio').masonry({
            itemSelector: '.portfolio-item',
            percentPosition: true
        });
        $("body").imagesLoaded(function(){
            $(".page-loader div").fadeOut();
            $(".page-loader").delay(200).fadeOut("slow");
        });
        
        var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
        if ($("#main").length) {
            var main = $("#main").offset().top;
    		$body.animate({
    			scrollTop: main + 1 + "px"
    		}, 10);
        }
    });
    
    $.each($('.side_menu li.has_sub > .sub_menu2'), function() {
        var tab_h = $(this).outerHeight();
        $(this).css({
            'margin-bottom': - tab_h + 'px'
        });
    });
    $('.topic_tab .nav-content').delegate('.tab-pane', 'change', function() {
        $.each(x, function() {
            $(this).css('height', 'auto');
            var i = 0;
            $.each($(this), function() {
                var h = $(this).outerHeight();
                if (i < h) {
                    i = h;
                } else {
                    i = i;
                }
            });
            $(this).css('height', i + 'px');
        });

        $.each(y, function() {
            $(this).css('height', 'auto');
            var j = 5000;
            $.each($(this), function() {
                var h = $(this).outerHeight();
                if (j > h) {
                    j = h;
                } else {
                    j = j;
                }
            });
            $(this).css('height', j + 'px');
        });
    });

    // $(".top_bar").delegate("#top_more_link", "click", function(a) {
    //     return a.preventDefault(), a.stopPropagation(), $(this).next().is(":hidden") ? $(this).next().fadeIn() : $(this).next().fadeOut();
    // });
    
    
    var tc = 6;
    var vc = 6;

    function tshow() {
        var i = 0;
        $("#topic .row > div").each(function() {
            if(i < tc) {
                $(this).css("display", "block");
            } else {
                $(this).css("display", "none");
            }
            i++;
        });
        if(tc >= i) {
            $("#topic .read_more").addClass("hidden")
        }
    }
    
    function vshow() {
        var i = 0;
        $("#video_zone .row > div").each(function() {
            if(i < vc) {
                $(this).css("display", "block");
            } else {
                $(this).css("display", "none");
            }
            i++;
        });
        if(vc >= i) {
            $("#video_zone .read_more").addClass("hidden")
        }
    }

    tshow();
    vshow();
        
    $("#topic .read_more").click(function() {
        tc += 6;
        tshow();
    });
    
    $("#video_zone .read_more").click(function() {
        vc += 6;
        vshow();
    });

    // header
    // $(window).bind("load resize", function() {
    //     var window_w = $(window).outerWidth();
    //     var container_w = $(".header_top .container").outerWidth();
    //     var header_h = $(".header_top").outerHeight();
    //     $(".header_bg").css("width", ((window_w - container_w)/2 - 20) + "px");
    //     if(window_w > 767) {
    //         $("#index_slider").css("margin-top", header_h + "px");
    //     } else {
    //         $("#index_slider").css("margin-top", "0px");
    //     }
    // });

    // top_bar
    $(".top_bar").delegate(".pull_btn", "click", function() {
        var top_bar_h = $(".top_bar").outerHeight();
        if($(this).parents(".top_bar").hasClass("top_bar_close")) {
            $(this).parents(".top_bar").removeClass("top_bar_close").css("margin-top", "0px");
        } else {
            $(this).parents(".top_bar").addClass("top_bar_close").css("margin-top", (-top_bar_h + 9) + "px");
        }
    });

    $(window).bind("load resize scroll", function() {
        var window_h = $(window).outerHeight();
        var nav_top = $(".navbar").offset().top;
        var scroll_top = $(document).scrollTop();
        if(nav_top - scroll_top + 30 < window_h / 2) {
            $(".navbar").removeClass("dropup");
        } else {
            $(".navbar").addClass("dropup");
        }
    });

    // $(document).ready(function() {
    //     $("body").css("opacity", "1");
    // });
    
    // $('#index_slider').owlCarousel({
    //     items: 1,
    //     loop: true,
    //     margin: 0,
    //     nav: true,
    //     navText: ["",""],
    //     dots: true,
    //     smartSpeed: 600,
    //     autoplay: true,
    //     autoplayTimeout: 10000,
    //     autoplayHoverPause: true
    // });

    $(".menu_slider > .owl-carousel").owlCarousel({
        items: 4,
        margin: 1,
        loop: true,
        nav: true,
        navText: ["",""],
        dots: false,
        smartSpeed: 600,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: true,
        thumbs: false,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            1200: {
                items: 4
            }
        },
        lazyLoad: true
    });

    $(".topic_slider > .owl-carousel").owlCarousel({
        items: 4,
        margin: 20,
        loop: true,
        nav: true,
        navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        dots: false,
        smartSpeed: 600,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: true,
        thumbs: false,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            }
        },
        lazyLoad: true
    });
    
    $(".event_slider > .owl-carousel").owlCarousel({
        items: 1,
        margin: 20,
        loop: true,
        nav: true,
        navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        dots: false,
        smartSpeed: 600,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: true,
        thumbs: false,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 1
            }
        },
        lazyLoad: true
    });
    
    $(".video_slider > .owl-carousel").owlCarousel({
        items: 1,
        margin: 20,
        loop: true,
        nav: true,
        navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        dots: false,
        smartSpeed: 600,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: true,
        thumbs: false,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            992: {
                items: 1
            }
        },
        lazyLoad: true
    });
    
    var news_owl = $(".news_slider > .owl-carousel");
    news_owl.owlCarousel({
        items: 1,
        margin: 0,
        loop: true,
        nav: false,
        dots: false,
        smartSpeed: 600,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:false,
        thumbs: false,
        lazyLoad: true,
        // autoHeight: true
    });
    $('.news_play').on('click',function(){
        news_owl.trigger('play.owl.autoplay',[1000]);
        $(this).parent(".news_controll").addClass("playing")
    });
    $('.news_stop').on('click',function(){
        news_owl.trigger('stop.owl.autoplay');
        $(this).parent(".news_controll").removeClass("playing");
    });

    $(".snap_slider > .owl-carousel").owlCarousel({
        items: 4,
        margin: 26,
        loop: true,
        nav: true,
        navText: ["",""],
        dots: false,
        smartSpeed: 600,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: true,
        thumbs: false,
        responsive: {
            0: {
                items: 1
            },
            550: {
                items: 2
            },
            992: {
                items: 3
            }
        },
        lazyLoad: true
    });

    // $(".gallery_slider.two_item > .owl-carousel").owlCarousel({
    //     items: 4,
    //     margin: 30,
    //     loop: true,
    //     nav: true,
    //     navText: ["<img src='assets/images/arrow_left.jpg' alt='nav_pre'>","<img src='assets/images/arrow_right.jpg' alt='nav_next'>"],
    //     dots: false,
    //     smartSpeed: 600,
    //     autoplay: true,
    //     autoplayTimeout: 10000,
    //     autoplayHoverPause: true,
    //     thumbs: false,
    //     responsive: {
    //         0: {
    //             items: 1
    //         },
    //         481: {
    //             items: 2
    //         }
    //     }
    // });

    $(".news").mCustomScrollbar({
        scrollButtons: {
            enable: false
        },
        theme: "light-thick",
        scrollbarPosition: "outside"
    });
    
    $('.ws-title').delegate('div', 'click', function() {
        window.location.href = $(this).find('a').attr('href');
    });

    $(".shortcut_plus").click(function() {
        if ($(this).parents("li.un-scale").hasClass("open")) {
            $(this).parents("li.un-scale").removeClass("open");
        } else {
            $(this).parents("li.un-scale").addClass("open");
            $(this).parents("li.un-scale").siblings().removeClass("open");
        }
        return false;
    });

    $(".slide_control").click(function() {
        if ($(this).parent(".index_slider").hasClass("slide_close")) {
            $(this).parent(".index_slider").removeClass("slide_close");
        } else {
            $(this).parent(".index_slider").addClass("slide_close");
        }
        return false;
    });

    $(".news_tab .nav-tabs a").click(function() {
        var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
        var offset_top = $(this).offset().top;
        $body.animate({
            scrollTop: (offset_top - 40) + 'px'
        }, 500, 'swing');
    });

    // nav
    $(window).bind("load resize scroll", function() {
        var navTop = $(".header_bottom").offset().top,
            hb_h = $(".header_bottom").outerHeight(),
            nav_h = $(".navbar").outerHeight(),
            scrollTop = $(window).scrollTop();
        if(scrollTop > navTop) {
            $("header").addClass("header_fixed");
            $(".header_bottom").css("height", hb_h + "px");
        } else {
            $("header").removeClass("header_fixed");
            $(".header_bottom").css("height", "auto");
        }
        $("#main").css("top", - nav_h + "px");

        // var tbh = $(".header_top").outerHeight();
        // var slider_h = $("#index_slider").outerHeight();
        // var w = $(window).width();
        // if(w <= 767) {
        //     if ($(this).scrollTop() >= (tbh + slider_h + 15)) {
        //         $("header").addClass("header_fixed");
        //     } else {
        //         $("header").removeClass("header_fixed");
        //     }
        // }
    });

    // $('.side_menu').delegate('li.has_sub > a', 'click', function(e) {
    //     e.preventDefault();
    //     if ($(this).parent('li').hasClass('active')) {
    //         $(this).parent('li').removeClass('active').find('ul').slideUp();
    //     } else {
    //         $(this).parent('li').addClass('active').find('ul').slideDown().parent('li').siblings().removeClass('active').find('ul').slideUp();
    //     }
    // });
    $('.side_menu').delegate('li.has_sub > a', 'click', function(e) {
        e.preventDefault();
        if ($(this).parent('li').hasClass('active')) {
            $(this).parent('li').removeClass('active');
        } else {
            $(this).parent('li').addClass('active').siblings().removeClass('active');
        }
    });

    // totop
    $(window).bind("load scroll", function() {
        var w_h = $(window).outerHeight();
        if ($(this).scrollTop() >= w_h / 2) {
            $(".totop").css({
                "opacity": "1",
                "visibility": "visible"
            });
        } else {
            $(".totop").css({
                "opacity": "0",
                "visibility": "hidden"
            });
        }
    });
    $('.totop').click(function() {
        var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
        $body.animate({
            scrollTop: $('header').offset().top
        }, 500);
        return false;
    });

    $('[data-toggle="dropdown"]').click(function(e) {
        if ($(window).width() < 753) {
            e.preventDefault();
        }
    });
    $('.dropdown-menu').click(function(e) {
        if ($(window).width() < 753) {
            e.stopPropagation();
        }
    });
    $('.dropdown-submenu').click(function(e) {
        if ($(window).width() < 753) {
            e.stopPropagation();
            e.preventDefault();
        }
    });
    $('.dropdown-submenu > a').click(function(e) {
        if ($(this).parent().hasClass('open')) {
            $(this).attr("aria-expanded", "false").parent().removeClass('open');
        } else {
            $(this).attr("aria-expanded", "true").parent().addClass('open');
        }
    });
    
    // $("img.lazyload").lazyload();

})(jQuery);

$(function() {
    $.fn.extend({
        treed: function(o) {

            var openedClass = 'glyphicon-plus-sign';
            var closedClass = 'glyphicon-minus-sign';

            if (typeof o != 'undefined') {
                if (typeof o.openedClass != 'undefined') {
                    openedClass = o.openedClass;
                }
                if (typeof o.closedClass != 'undefined') {
                    closedClass = o.closedClass;
                }
            };

            var tree = $(this);
            tree.addClass("tree");
            tree.find('li').has("ul").each(function() {
                var branch = $(this); //li with children ul
                branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
                branch.children('a').append(" <span class='caret' style='display: inline-block;'></span>");
                branch.addClass('branch');
                branch.on('click', function(e) {
                    if (this == e.target) {
                        var icon = $(this).children('i:first');
                        icon.toggleClass(openedClass + " " + closedClass);
                        $(this).children().children().toggle();
                    }
                })
            });

            tree.find('.branch .indicator').each(function() {
                $(this).on('click', function() {
                    $(this).closest('li').click();
                });
            });

            tree.find('.branch>button').each(function() {
                $(this).on('click', function(e) {
                    $(this).closest('li').click();
                    e.preventDefault();
                });
            });
        }
    });

    $('.tree').treed();
    
    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: false
    });
    
    $('.news_slider').flexslider({
        animation: "slide",
        controlNav: false,
        slideshowSpeed: 3000
    });
});

(function($) {
    function findChild(parent, index) {
        $(parent).children("a").prepend(index);
        $(parent).children("ul").children('li').each(function(index1) {
            index1 = index1 + 1;
            var str = index + ' - ' + index1;
            findChild(this, str);
        });
    }
    $('ul.tree > li').each(function(index) {
        findChild(this, index + 1);
    });
});
