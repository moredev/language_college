(function() {
  window.Tool = (function() {
    var tool,
      _this = this;
    tool = {
      CDN_jQuery: '//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js',
      CDN_Underscore: '//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.4/underscore-min.js',
      totalJS: 2,
      loadedJS: 0,
      init: function() {},
      template: function(_tpl_dir, _tpl_name, _data, callback) {
        var renderCache, tmpl_url, _htmlStr,
          _this = this;
        if (_data == null) {
          _data = {
            items: null
          };
        }
        _data["_"] = _;
        if (!window.renderCache) {
          renderCache = {};
        }
        if (renderCache[_tpl_name] != null) {
          _htmlStr = _.template(renderCache[_tpl_name], _data);
          return callback(_htmlStr);
        } else {
          tmpl_url = _tpl_dir + '/' + _tpl_name + '.html';
          return $.ajax({
            url: tmpl_url,
            method: 'GET',
            dataType: 'text',
            contentType: 'text',
            async: false,
            cache: false,
            success: function(result) {
              renderCache[_tpl_name] = _.template(result.replace(/[\r\n\t]/g, ""));
              _htmlStr = renderCache[_tpl_name](_data);
              return callback(_htmlStr);
            }
          });
        }
      },
      easyTemplate: function(selector, _tpl_url, _data, callback) {
        var renderCache, _htmlStr,
          _this = this;
        if (_data == null) {
          _data = null;
        }
        if (callback == null) {
          callback = null;
        }
        if (_data == null) {
          _data = {
            items: null
          };
        }
        _data["_"] = _;
        _data["Tool"] = Tool;
        if (!window.renderCache) {
          renderCache = {};
        }
        if (renderCache[_tpl_url] != null) {
          _htmlStr = _.template(renderCache[_tpl_url], _data);
          $(selector).empty().append(_htmlStr);
          if (callback != null) {
            return callback(_htmlStr);
          }
        } else {
          return $.ajax({
            url: _tpl_url,
            method: 'GET',
            dataType: 'text',
            contentType: 'text',
            async: false,
            cache: false,
            success: function(result) {
              renderCache[_tpl_url] = _.template(result.replace(/[\r\n\t]/g, ""));
              _htmlStr = renderCache[_tpl_url](_data);
              $(selector).empty().append(_htmlStr);
              if (callback != null) {
                return callback(_htmlStr);
              }
            }
          });
        }
      },
      trace: function(_value) {
        if (typeof _value === 'string') {
          return window.console && console.log(_value || (document.title = _value));
        } else {
          if (window.console) {
            return console.log(_value);
          } else {
            return document.title = _value;
          }
        }
      },
      loadScript: function(url, callback) {
        var script,
          _this = this;
        if (callback == null) {
          callback = null;
        }
        if (!window.urlCache) {
          window.urlCache = {};
        }
        if (urlCache[url] == null) {
          urlCache[url] = url;
          script = document.createElement("script");
          script.type = "text/javascript";
          if (script.readyState) {
            script.onreadystatechange = function() {
              if (script.readyState === "loaded" || script.readyState === "complete") {
                script.onreadystatechange = null;
                if (callback != null) {
                  return callback();
                }
              }
            };
          } else {
            script.onload = function() {
              if (callback != null) {
                return callback();
              }
            };
          }
          script.src = url;
          return document.getElementsByTagName("head")[0].appendChild(script);
        } else {
          if (callback != null) {
            return callback();
          }
        }
      },
      loadMultiScript: function(_array, callback, isFirst) {
        var _this = this;
        if (callback == null) {
          callback = null;
        }
        if (isFirst == null) {
          isFirst = true;
        }
        if (isFirst) {
          this.loadedJS = 0;
          this.totalJS = _array.length;
        }
        if (_array.length > 0) {
          return this.loadScript(_array[this.loadedJS], function() {
            _this.loadedJS++;
            if (_this.loadedJS === _this.totalJS) {
              return callback();
            } else {
              return _this.loadMultiScript(_array, callback, false);
            }
          });
        }
      },
      ready: function(callback) {
        var _checkid,
          _this = this;
        return _checkid = setInterval(function() {
          if (_this.loadedJS === _this.totalJS) {
            callback();
            return clearInterval(_checkid);
          }
        }, 1000);
      }
    };
    if (typeof $ === 'undefined') {
      tool.loadScript(tool.CDN_jQuery, function() {
        return tool.loadedJS++;
      });
    } else {
      tool.loadedJS++;
    }
    if (typeof _ === 'undefined') {
      tool.loadScript(tool.CDN_Underscore, function() {
        return tool.loadedJS++;
      });
    } else {
      tool.loadedJS++;
    }
    return tool;
  })();

}).call(this);
