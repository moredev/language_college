<?php

return [
    'permissions' => [
        'manage_software_updates' => '管理軟體更新',
    ],
    'event_log' => [
        'hint' => '日誌顯示了程式中的潛在錯誤, 比如異常和測試訊息.',
    ],
    'updates' => [
        'name' => '軟體更新',
    ],
];
