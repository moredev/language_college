<?php

return [
   'media' => [
        'filter_video' => '影像檔',
        'filter_audio' => '音檔',
        'filter_documents' => '文件',
   ],
];
